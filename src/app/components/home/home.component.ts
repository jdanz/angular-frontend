import { Component, OnInit } from '@angular/core';
import {Articulo} from '../../models/articulo';
import {ArticuloService} from '../../services/articulo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  articulos: Articulo[] = new Array();
    constructor(private articuloService: ArticuloService) { }

  ngOnInit() {
      this.articuloService.listar().subscribe(resp => {
        this.articulos = resp;
      });
  }
  /*
  articulos = [{
    'nombre': 'Nombre del artículo',
    'descripcion': 'Descripción del artículo',
    'precio': 20.00,
    'cantidad': 50,
    'url': 'https://images-na.ssl-images-amazon.com/images/I/91uSBVvhggL._UY606_.jpg',
    'descuento': 15
  }, {
    'nombre': 'Nombre del artículo 2',
    'descripcion': 'Descripción 2',
    'precio': 10.00,
    'cantidad': 30,
    'url': 'https://images-na.ssl-images-amazon.com/images/I/91uSBVvhggL._UY606_.jpg',
    'descuento': 12
  }];
  */


}
