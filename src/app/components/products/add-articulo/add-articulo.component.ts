import {AfterContentInit, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {min} from 'rxjs/operators';
import {Articulo} from '../../../models/articulo';
import {ArticuloService} from '../../../services/articulo.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-add-articulo',
  templateUrl: './add-articulo.component.html',
  styleUrls: ['./add-articulo.component.scss']
})
export class AddArticuloComponent implements OnInit, AfterContentInit {
  articulo: Articulo = {};
  categorias: any[] = [{
    id: 'CAT_CEL',
    nombre: 'Celulares'
    }, {
    id: 'CAT 2',
    nombre: 'Categoría UNO'
  }
  ];
  articuloForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private articuloService: ArticuloService,
    private dialogRef: MatDialogRef<AddArticuloComponent>,
    @Inject(MAT_DIALOG_DATA)public data: any) {}
  ngAfterContentInit(): void {
    this.articulo = this.data.articulo;
    this.articuloForm = this.formBuilder.group({
      id: [this.articulo.id],
      nombre: [this.articulo.nombre, [Validators.required, Validators.minLength(5)]],
      descripcion: [this.articulo.descripcion],
      precio: [this.articulo.precio, Validators.required],
      cantidad: [this.articulo.cantidad],
      url: [this.articulo.url, Validators.required],
      descuento: [this.articulo.descuento],
      categoria: [this.articulo.categoria.id],
    });
  }

  ngOnInit() {
    this.articuloForm = this.formBuilder.group({
      id: [''],
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      descripcion: [''],
      precio: ['', Validators.required],
      cantidad: [''],
      url: ['', Validators.required],
      descuento: [''],
      categoria: [''],
    });
  }

  guardar(): void {
    const articulo: Articulo = {
      nombre: this.articuloForm.get('nombre').value,
      descripcion: this.articuloForm.get('descripcion').value,
      precio: this.articuloForm.get('precio').value,
      cantidad: this.articuloForm.get('cantidad').value,
      url: this.articuloForm.get('url').value,
      descuento: this.articuloForm.get('descuento').value,
      categoria:{
        id: this.articuloForm.get('categoria').value
      }
    };

    if(this.articuloForm.get('id') && this.articuloForm.get('id').value){
      articulo.id = this.articuloForm.get('id').value;
    }
    this.articulo = articulo;

    if (this.articulo.id) {
      this.articuloService.actualizar(articulo).subscribe(resp =>{
        this.dialogRef.close(articulo);
      });
    } else {
      this.articuloService.guardar(articulo).subscribe(resp => {
        this.dialogRef.close(articulo);
      });
    }
  }
}
