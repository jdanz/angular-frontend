import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor( private formBuilder: FormBuilder, private loginService: LoginService, ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  login(): void {
    const username = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    this.loginService.login(username, password).subscribe(
      resp => {
        console.log(resp);
      });
  }
}
