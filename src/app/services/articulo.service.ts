import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Articulo} from '../models/articulo';
import {HOST} from '../variables/var.constant';

@Injectable({
  providedIn: 'root'
})

export class ArticuloService {
  url: string = HOST + '/api/articulos/';
  constructor(private http: HttpClient) {}
  listar(): Observable<Articulo[]> {
    return this.http.get<Articulo[]>(this.url);
  }

  guardar(articulo: Articulo): Observable<Articulo> {
    return this.http.post<Articulo>(this.url, articulo);
  }
  actualizar(articulo: Articulo): Observable<Articulo> {
    return this.http.put<Articulo>(this.url, articulo, {headers: this.getHeaders()});
  }
  getHeaders(): HttpHeaders {
    let headers = new HttpHeaders;
    // tslint:disable-next-line:max-line-length
    const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidGllbmRhcmVzb3VyY2VpZCJdLCJ1c2VyX25hbWUiOiJrdmFuZXNjQHZhbmUuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sImV4cCI6MTU0ODMxNTMyNiwiYXV0aG9yaXRpZXMiOlsiQWRtaW5pc3RyYXRvciJdLCJqdGkiOiIwMWQ2NTFkYy1iMzMwLTRiYmQtYTk1ZS03NDMyZDhjNGNjNTMiLCJjbGllbnRfaWQiOiJ0aWVuZGFtZWRpYXBwIn0.PlhMI75xBMo7FrK_syx0Bk5nFZ3cLjFfSws4bh9148Y';
    headers = headers.append('Content-Type', 'application/json');
    if (token !== null) {
      headers = headers.append('Authorization', token);
    }
    return headers;
  }

}

